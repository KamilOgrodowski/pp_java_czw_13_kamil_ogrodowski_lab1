public class Fibb {

 public final static int STALA = 4000000;

public static int elementCiaguFibb(int n){
		if(n < 3) return 1;
		return elementCiaguFibb(n-2)+elementCiaguFibb(n-1);
	}
	
	public static void main(String[] args) {
		int WYNIK = 0;
		for(int i = 1;i<STALA;i++){
			if(elementCiaguFibb(i) < STALA){ 
				if(elementCiaguFibb(i)%2==0) WYNIK+=elementCiaguFibb(i);
			}
			else{break;}
		}
		System.out.print("Suma wynosi ");
		System.out.print(WYNIK);
	}
}

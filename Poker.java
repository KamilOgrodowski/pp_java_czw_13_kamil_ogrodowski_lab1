import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Poker {
	static int[] pokerKrolewski = {10, 11, 12, 13, 14};
	static int[][] strit = {{2, 3, 4, 5, 14}, {2,3,4,5,6}, {3,4,5,6,7}, {4,5,6,7,8}, {5,6,7,8,9,10}, {6,7,8,9,10,11},
			{7,8,9,10,11}, {8,9,10,11,12}, {9,10,11,12,13}, {10,11,12,13,14}};
	
	public static class Karta{
		public	int wartosc;
		public	String kolor;
		public Karta(String tablica){ 
			
		if(tablica.charAt(0) == 'T') this.wartosc = 10;
			else if(tablica.charAt(0) == 'J') this.wartosc = 11;
				else if((tablica.charAt(0)) == 'Q') this.wartosc = 12;
					else if((tablica.charAt(0)) == 'K') this.wartosc = 13;
						else if((tablica.charAt(0)) =='A') this.wartosc = 14;
							else this.wartosc =	(int)(tablica.charAt(0)-48);
		this.kolor = Character.toString((tablica.charAt(1)));
		
		}
		
	}
	public static class Rozgrywka{
		private static	Karta[] gracz1 = new Karta[5];
		private	static  Karta[] gracz2 = new Karta[5];
		public void posortujKarty(){
		    int temp = 0;
		    String kolor;
		    for (int i = 0; i < 5; i++) {
		        for (int j = 1; j < (5 - i); j++) {
		            if (gracz1[j - 1].wartosc > gracz1[j].wartosc) {
		                temp = gracz1[j - 1].wartosc;
		                kolor = gracz1[j - 1].kolor;
		                gracz1[j - 1].wartosc = gracz1[j].wartosc;
		                gracz1[j - 1].kolor = gracz1[j].kolor;
		                gracz1[j].wartosc = temp;
		                gracz1[j].kolor = kolor;
		            }

		        }
		    }
		    for (int i = 0; i < 5; i++) {
		        for (int j = 1; j < (5 - i); j++) {
		        	if (gracz2[j - 1].wartosc > gracz2[j].wartosc) {
		                temp = gracz2[j - 1].wartosc;
		                kolor = gracz2[j - 1].kolor;
		                gracz2[j - 1].wartosc = gracz2[j].wartosc;
		                gracz2[j - 1].kolor = gracz2[j].kolor;
		                gracz2[j].wartosc = temp;
		                gracz2[j].kolor = kolor;
		            }

		        }
		    }
		}
		public void wypiszKarty(){
			System.out.print("Gracz1 :");
			for(int i = 0; i<5; i++){
				System.out.print(gracz1[i].wartosc);
				System.out.print(gracz1[i].kolor);
				System.out.print(" ");
				}
			System.out.print("\n");
			System.out.print("Gracz2 :");
			for(int i = 0; i<5; i++){
				System.out.print(gracz2[i].wartosc);
				System.out.print(gracz2[i].kolor);
				System.out.print(" ");
				}
			System.out.print("\n");
		}
		public int zdecydujKtoWygral(){
			int punktyGracza1 = 0;
			int punktyGracza2 = 0;
					if(intNaBoolean(sprawdzPokerKrolewski(gracz1))) punktyGracza1 = 10;
					else if(intNaBoolean(sprawdzPoker(gracz1))) punktyGracza1 = 9;
					else if(intNaBoolean(sprawdzKarete(gracz1))) punktyGracza1 = 8;
					else if(intNaBoolean(sprawdzFul(gracz1))) punktyGracza1 = 7;
					else if(intNaBoolean(sprawdzKolor(gracz1))) punktyGracza1 = 6;
					else if(intNaBoolean(sprawdzStrit(gracz1))) punktyGracza1 = 5;
					else if(intNaBoolean(sprawdzTrojka(gracz1))) punktyGracza1 = 4;
					else if(intNaBoolean(sprawdzDwiePary(gracz1,2))) punktyGracza1 = 3;
					else if(intNaBoolean(sprawdzPare(gracz1))) punktyGracza1 =2;
					else  punktyGracza1 = 1;
					
					if(intNaBoolean(sprawdzPokerKrolewski(gracz2))) punktyGracza2 = 10;
					else if(intNaBoolean(sprawdzPoker(gracz2))) punktyGracza2 = 9;
					else if(intNaBoolean(sprawdzKarete(gracz2))) punktyGracza2 = 8;
					else if(intNaBoolean(sprawdzFul(gracz2))) punktyGracza2 = 7;
					else if(intNaBoolean(sprawdzKolor(gracz2))) punktyGracza2 = 6;
					else if(intNaBoolean(sprawdzStrit(gracz2))) punktyGracza2 = 5;
					else if(intNaBoolean(sprawdzTrojka(gracz2))) punktyGracza2 = 4;
					else if(intNaBoolean(sprawdzDwiePary(gracz2,2))) punktyGracza2 = 3;
					else if(intNaBoolean(sprawdzPare(gracz2))) punktyGracza2 =2;
					else punktyGracza2 = 1;
					
					if(punktyGracza1 > punktyGracza2) return 1;
					else if (punktyGracza1 < punktyGracza2) return 2;
					else 
					{
						switch(punktyGracza1){
						case 9: return sprawdzPoker(gracz1) > sprawdzPoker(gracz2)?  1 :  2;
						case 8: return sprawdzKarete(gracz1) > sprawdzKarete(gracz2)? 1 : 2;
						case 7: return sprawdzFul(gracz1) > sprawdzFul(gracz2) ? 1 : 2; 
						case 6: 
								if(sprawdzKolor(gracz1) == sprawdzKolor(gracz2)){
									int n = sprawdzNajwyzszaKarta(gracz1,gracz2, 1);
									switch(n){
									case -1: return -1;
									case 1: return 1;
									case 2: return 2;
									}
								}
								else return sprawdzKolor(gracz1) > sprawdzKolor(gracz2) ? 1 : 2;
								
						case 5: if(sprawdzStrit(gracz1) == sprawdzStrit(gracz2)) return -1;
								else return sprawdzStrit(gracz1) > sprawdzStrit(gracz2) ? 1 : 2 ;
						case 4: return sprawdzTrojka(gracz1) > sprawdzTrojka(gracz2) ? 1 : 2;
						case 3: if(sprawdzDwiePary(gracz1,2) == sprawdzDwiePary(gracz2,2)){
									if(sprawdzDwiePary(gracz1,1) == sprawdzDwiePary(gracz2,1)){
										if(sprawdzDwiePary(gracz1,0) == sprawdzDwiePary(gracz2,0)){
											return -1;
										}
										else return sprawdzDwiePary(gracz1,0) > sprawdzDwiePary(gracz2,0) ? 1 : 2;
									}
									else return sprawdzDwiePary(gracz1,1) > sprawdzDwiePary(gracz2,1) ? 1 : 2;
								}
								else return sprawdzDwiePary(gracz1,2) > sprawdzDwiePary(gracz2,2) ? 1 : 2;
						
						case 2: 
							if(sprawdzPare(gracz1) == sprawdzPare(gracz2)){
							int n = sprawdzNajwyzszaKarta(gracz1,gracz2, 0);
								switch(n){
								case -1: return -1;
								case 1: return 1;
								case 2: return 2;
								}
							}
							else return sprawdzPare(gracz1) > sprawdzPare(gracz2) ? 1 : 2;
						case 1: int n = sprawdzNajwyzszaKarta(gracz1,gracz2, 0);
								switch(n){
								case -1: return -1;
								case 1: return 1;
								case 2: return 2;
								}
						default: return -2;
						
							}
		}
	}

	public static boolean intNaBoolean(int a) {
	    if (a == 0)
	        return false;
	    return true;
	}
	public static int sprawdzKolor(Karta[] tablica) {
		if(tablica[0].kolor.equals(tablica[1].kolor)&&tablica[0].kolor.equals(tablica[2].kolor)&&tablica[0].kolor.equals(tablica[3].kolor)&&tablica[0].kolor.equals(tablica[4].kolor))
			return tablica[4].wartosc;
		else 
			return 0;
}
	public static int sprawdzPokerKrolewski(Karta[] tablica) {
			if(intNaBoolean(sprawdzKolor(tablica))){
					for(int i = 0; i<tablica.length; i++){
						if(tablica[i].wartosc != pokerKrolewski[i])
							return 0;
					}
					return tablica[4].wartosc;
			}
			return 0;

	}
	public static int sprawdzStrit(Karta[] tablica){
			for(int i = 0; i<10;i++){
				for(int j = 0; j<5; j++){
					if(tablica[j].wartosc != strit[i][j] ) break;
					if(j == 4){
						if(tablica[4].wartosc == 14 && tablica[3].wartosc == 5) return 5;
						else return tablica[4].wartosc;
					}
				}
			}
				return 0;
	}
	public static int sprawdzPoker(Karta[] tablica){
		if(intNaBoolean(sprawdzKolor(tablica))){
			if(intNaBoolean(sprawdzStrit(tablica))) return sprawdzStrit(tablica);
			else return 0;
			}
		return 0;
	}
	public static int sprawdzKarete(Karta[] tablica) {
		if(tablica[0].wartosc == tablica[1].wartosc && tablica[1].wartosc == tablica[2].wartosc
				&& tablica[2].wartosc == tablica[4].wartosc) return tablica[0].wartosc;
		else if(tablica[1].wartosc == tablica[2].wartosc && tablica[2].wartosc == tablica[3].wartosc
				&& tablica[3].wartosc == tablica[4].wartosc) return tablica[1].wartosc;
		else return 0;
}
	public static int sprawdzFul(Karta[] tablica){
			if(tablica[0].wartosc == tablica[1].wartosc && tablica[1].wartosc == tablica[2].wartosc
					&& tablica[3].wartosc == tablica[4].wartosc) return tablica[0].wartosc;
			else if(tablica[2].wartosc == tablica[3].wartosc && tablica[3].wartosc == tablica[4].wartosc
					&& tablica[0].wartosc == tablica[1].wartosc) return tablica[2].wartosc;
			else return 0;
		
	}
	public static int sprawdzTrojka(Karta[] tablica){ 
		//mozna miec tylko jedna trojke takich samych kart wiec zawsze znamy od razu zwyciezce
			if(tablica[0].wartosc == tablica[1].wartosc && tablica[1].wartosc == tablica[2].wartosc) 
				return tablica[0].wartosc;
			else if(tablica[1].wartosc == tablica[2].wartosc && tablica[2].wartosc == tablica[3].wartosc)
				 return tablica[1].wartosc;
			else if(tablica[2].wartosc == tablica[3].wartosc && tablica[3].wartosc == tablica[4].wartosc) 
				return tablica[2].wartosc;
			else return 0;
		}
	public static int sprawdzDwiePary(Karta[] tablica, int Wysoka){ 
		// dwa zwraca wartosc wy�szej dwojki,
		// jeden zwraca wartosc nizszej dwojki
		// 0 zwraca wartosc ostatniej karty
		if(Wysoka == 2){
			if(tablica[0].wartosc == tablica[1].wartosc && tablica[2].wartosc == tablica[3].wartosc) 
				return tablica[2].wartosc;
			else if(tablica[0].wartosc == tablica[1].wartosc && tablica[3].wartosc == tablica[4].wartosc)
				return tablica[3].wartosc;
			else if(tablica[1].wartosc == tablica[2].wartosc && tablica[3].wartosc == tablica[4].wartosc) 
				return tablica[3].wartosc;
			else return 0;
		}
		else if(Wysoka == 1){
			if(tablica[0].wartosc == tablica[1].wartosc && tablica[2].wartosc == tablica[3].wartosc) 
				return tablica[0].wartosc;
			else if(tablica[0].wartosc == tablica[1].wartosc && tablica[3].wartosc == tablica[4].wartosc)
				 return tablica[0].wartosc;
			else if(tablica[1].wartosc == tablica[2].wartosc && tablica[3].wartosc == tablica[4].wartosc) 
				return tablica[3].wartosc;
			else return 0;
			}
		else{
			if(tablica[0].wartosc == tablica[1].wartosc && tablica[2].wartosc == tablica[3].wartosc) 
				return tablica[4].wartosc;
			else if(tablica[0].wartosc == tablica[1].wartosc && tablica[3].wartosc == tablica[4].wartosc)
				 return tablica[2].wartosc;
			else if(tablica[1].wartosc == tablica[2].wartosc && tablica[3].wartosc == tablica[4].wartosc) 
				return tablica[0].wartosc;
			else return 0;
		}
	}
	public static int sprawdzPare(Karta[] tablica){
		if(tablica[0].wartosc == tablica[1].wartosc) 
			return tablica[0].wartosc;
		else if(tablica[1].wartosc == tablica[2].wartosc)
			 return tablica[1].wartosc;
		else if(tablica[2].wartosc == tablica[3].wartosc) 
			return tablica[2].wartosc;
		else if(tablica[3].wartosc == tablica[4].wartosc) 
			return tablica[3].wartosc;
		else return 0;
	}
	 public static int sprawdzNajwyzsza(Karta[] tablica, int odejmij) {            
         return tablica[4-odejmij].wartosc;
	 }
         public static int sprawdzNajwyzszaKarta(Karta[] tablica1, Karta[] tablica2, int i){
    	 if(sprawdzNajwyzsza(tablica1,i) == sprawdzNajwyzsza(tablica2,i)){
    		 if(i<5) return sprawdzNajwyzszaKarta(tablica1,tablica2,i+1);
    		 else return -1;
    	 }
    	 else return sprawdzNajwyzsza(tablica1,i) > sprawdzNajwyzsza(tablica2,i) ? 1 : 2;
     }
		public void ustawKartyGraczy(String[] partia) {
			for(int i = 0; i<5; i++){
			gracz1[i] = new Karta(partia[i]);
			gracz2[i] = new Karta(partia[i+5]);			
			}

		}
	}
	public static void main(String[] args) throws IOException{
		FileReader fr = new FileReader("poker.txt");
		BufferedReader reader = new BufferedReader(fr);
		 
		int numerRozdania = 0;
		int licznikWygranychGracz1 = 0;
		int licznikWygranychGracz2 = 0;
		int wygrany = 0;
		
		String zestawKart;
		String[] partia;
		
		Rozgrywka poker = new Rozgrywka();
		
		while( (zestawKart = reader.readLine())!= null){
			partia = zestawKart.split(" ");
			poker.ustawKartyGraczy(partia);
			poker.posortujKarty();
			
			wygrany = poker.zdecydujKtoWygral();
			switch(wygrany){
			case -2: System.out.println("Niepoprawny wynik gracza"); break;
			case -1: System.out.println("Nie mozna stwierdzic kto wygral w rozdaniu "+numerRozdania); break;
			case 1:  licznikWygranychGracz1++; break;
			case 2:  licznikWygranychGracz2++; break;
			}
		}
		reader.close();
		System.out.println("Gracz1 wygral "+licznikWygranychGracz1+" razy");
		System.out.println("Gracz2 wygral "+licznikWygranychGracz2+" razy");
		
}
	}
